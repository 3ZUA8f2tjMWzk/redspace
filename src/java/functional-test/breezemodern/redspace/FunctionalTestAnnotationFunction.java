///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of redspace.
//
// redspace is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// redspace is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with redspace.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . redspace ;

import breezemodern . easterncrayon . UseConstructor ;
import breezemodern . easterncrayon . UseInstanceMethod ;
import breezemodern . easterncrayon . UseParameter ;
import breezemodern . easterncrayon . UseStringConstant ;
import com . google . common . base . Function ;
import java . io . Writer ;
import java . util . Map ;
import javax . annotation . processing . Filer ;
import javax . annotation . processing . ProcessingEnvironment ;
import javax . lang . model . element . Element ;
import javax . lang . model . element . TypeElement ;
import javax . tools . JavaFileObject ;

abstract class FunctionalTestAnnotationFunction implements Function < TypeElement , Object >
{
    @ Override
	public final Object apply ( final TypeElement annotation )
	{
	    if ( equals ( failure ( ) , get ( getOptions ( processingEnvironment ( ) ) , blueAutumn ( ) ) ) )
		{
		    close ( openWriter ( createSourceFile ( getFiler ( processingEnvironment ( ) ) , failureSource ( ) ) ) ) ;
		}
	    else if ( equals ( error ( ) , get ( getOptions ( processingEnvironment ( ) ) , blueAutumn ( ) ) ) )
		{
		    throw runtimeException ( ) ;
		}
	    else if ( ! equals ( coverage ( ) , get ( getOptions ( processingEnvironment ( ) ) , blueAutumn ( ) ) ) )
		{
		    coverageMethod ( ) ;
		}
	    return null ;
	}

    private void coverageMethod ( )
    {
    }

    @ UseInstanceMethod
	abstract < T , F > T apply ( Function < ? super T , ? extends F > function , F object ) ;

    @ UseStringConstant ( "failure" )
	abstract String failure ( ) ;

    @ UseStringConstant ( "breezemodern.blueautumn" )
	abstract String blueAutumn ( ) ;

    @ UseInstanceMethod
	abstract Map < String , String > getOptions ( ProcessingEnvironment processingEnvironment ) ;

    @ UseInstanceMethod
	abstract < K , V > V get ( Map < K , V > map , K key ) ;

    @ UseInstanceMethod
	abstract Boolean equals ( Object o1 , Object o2 ) ;

    @ UseStringConstant ( "failure.java" )
	abstract String failureSource ( ) ;

    @ UseParameter
	abstract ProcessingEnvironment processingEnvironment ( ) ;

    @ UseInstanceMethod
	abstract Filer getFiler ( ProcessingEnvironment processingEnvironment ) ;

    @ UseInstanceMethod
	abstract JavaFileObject createSourceFile ( Filer filer , CharSequence qualifiedName , Element ... elements ) ;

    @ UseInstanceMethod
	abstract Writer openWriter ( JavaFileObject file ) ;

    @ UseInstanceMethod
	abstract void close ( Writer writer ) ;

    @ UseStringConstant ( "error" )
	abstract String error ( ) ;

    @ UseConstructor ( RuntimeException . class )
	abstract RuntimeException runtimeException ( ) ;

    @ UseStringConstant ( "coverage" )
	abstract String coverage ( ) ;
}
