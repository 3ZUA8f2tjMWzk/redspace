///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of redspace.
//
// redspace is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// redspace is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with redspace.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . redspace ;

import breezemodern . easterncrayon . UseInstanceMethod ;
import breezemodern . easterncrayon . UseParameter ;
import breezemodern . easterncrayon . UseStringConstant ;
import com . google . common . base . Predicate ;
import javax . lang . model . element . TypeElement ;

abstract class FunctionalTestAnnotationPredicate implements Predicate < TypeElement >
{
    @ Override
	public final boolean apply ( final TypeElement annotation )
    {
	return equals ( match ( ) , equals ( blueAutumn ( ) , toString ( getQualifiedName ( annotation ) ) ) ) ;
    }

    @ UseStringConstant ( "breezemodern.easterncrayon.BlueAutumn" )
	abstract Object blueAutumn ( ) ;

    @ UseInstanceMethod
	abstract Object getQualifiedName ( TypeElement annotation ) ;

    @ UseInstanceMethod
	abstract String toString ( Object object ) ;

    @ UseInstanceMethod
	abstract Boolean equals ( Object o1 , Object o2 ) ;

    @ UseParameter
	abstract Boolean match ( ) ;
}
