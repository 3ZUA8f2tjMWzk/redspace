///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of redspace.
//
// redspace is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// redspace is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with redspace.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . redspace ;

import breezemodern . easterncrayon . Implementation ;
import breezemodern . easterncrayon . UseConstructor ;
import breezemodern . easterncrayon . UseInstanceMethod ;
import java . util . Set ;
import javax . annotation . processing . AbstractProcessor ;
import javax . annotation . processing . ProcessingEnvironment ;
import javax . annotation . processing . RoundEnvironment ;
import javax . lang . model . element . TypeElement ;

@ Implementation ( "StandardRedSpaceProcessor" )
    abstract class RedSpaceProcessor extends AbstractProcessor
    {
	@ Override
	    public final boolean process ( final Set < ? extends TypeElement > annotations , final RoundEnvironment roundEnvironment )
	{
	    System . out . println ( "((((((((((((((((((((((((((((" ) ;
	    System . out . println ( processingService ( ) instanceof ProcessingService ) ;
	    System . out . println ( processingService ( ) instanceof FunctionalTestProcessingService ) ;
	    return process ( processingService ( ) , annotations , processingEnv , roundEnvironment ) ;
	}

	@ UseConstructor ( AbstractProcessingService . class )
	    abstract ProcessingService processingService ( ) ;

	@ UseInstanceMethod
	    abstract Boolean process ( ProcessingService processingService , Set < ? extends TypeElement > annotations , ProcessingEnvironment processingEnvironment , RoundEnvironment roundEnvironment ) ;
    }
