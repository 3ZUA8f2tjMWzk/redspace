///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of redspace.
//
// redspace is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// redspace is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with redspace.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . redspace ;

import breezemodern . easterncrayon . UseConstructor ;
import breezemodern . easterncrayon . UseInstanceMethod ;
import breezemodern . easterncrayon . UseParameter ;
import breezemodern . easterncrayon . UseStaticMethod ;
import com . google . common . base . Function ;
import com . google . common . collect . Collections2 ;
import java . util . Collection ;
import java . util . List ;
import javax . annotation . processing . ProcessingEnvironment ;
import javax . annotation . processing . RoundEnvironment ;
import javax . lang . model . element . Element ;
import javax . lang . model . element . TypeElement ;

abstract class AnnotationIterationFunction implements Function < TypeElement , Object >
{
    @ Override
	public final Object apply ( final TypeElement annotation )
	{
	    return apply ( summaryFunction ( ) , transform ( getElementsAnnotatedWith ( roundEnvironment ( ) , annotation ) , iterationFunction ( processingEnvironment ( ) , roundEnvironment ( ) ) ) ) ;
	}

    @ UseParameter
	abstract RoundEnvironment roundEnvironment ( ) ;

    @ UseInstanceMethod
	abstract List < ? extends Element > getElementsAnnotatedWith ( RoundEnvironment roundEnvironment , TypeElement annotation ) ;

    @ UseConstructor ( ElementFunction . class )
	abstract Function < ? super Element , ? > iterationFunction ( final ProcessingEnvironment processingEnvironment , final RoundEnvironment roundEnvironment ) ;

    @ UseParameter
	abstract ProcessingEnvironment processingEnvironment ( ) ;

    @ UseStaticMethod ( Collections2 . class )
	abstract < T , F > Collection < T > transform ( Collection < F > fromCollection , Function < ? super F , ? extends T > function ) ;

    @ UseConstructor ( ElementSummaryFunction . class )
	abstract Function < ? super Collection < ? > , ? > summaryFunction ( ) ;

    @ UseStaticMethod ( Collections2 . class )
	abstract < T , F > T apply ( Function < ? super F , ? extends T > function , F object ) ;
}
