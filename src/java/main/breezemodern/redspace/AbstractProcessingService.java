///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of redspace.
//
// redspace is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// redspace is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with redspace.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . redspace ;

import breezemodern . easterncrayon . UseConstructor ;
import breezemodern . easterncrayon . UseInstanceMethod ;
import breezemodern . easterncrayon . UseStaticMethod ;
import com . google . common . base . Function ;
import com . google . common . collect . Collections2 ;
import java . util . Collection ;
import javax . annotation . processing . ProcessingEnvironment ;
import javax . annotation . processing . RoundEnvironment ;
import javax . lang . model . element . TypeElement ;

public abstract class AbstractProcessingService implements ProcessingService
{
    @ Override
	public final Boolean process ( final Collection < ? extends TypeElement > annotations , final ProcessingEnvironment processingEnvironment , final RoundEnvironment roundEnvironment )
    {
	//	Boolean x = apply ( summaryFunction ( ) , transform ( annotations , iterationFunction ( processingEnvironment , roundEnvironment ) ) ) ;
	System . out . println ( "summaryFunction ( ) instanceof AnnotationSummaryFunction:  " + ( summaryFunction ( ) instanceof AnnotationSummaryFunction ) ) ;
	//	Boolean x = summaryFunction ( ) . apply ( transform ( annotations , iterationFunction ( processingEnvironment , roundEnvironment ) ) ) ;
	Boolean x = summaryFunction ( ) . apply ( null ) ;
	if ( null == x )
	    {
		throw new RuntimeException ( "ALPHA" ) ;
	    }
	return x ;
    }

    @ UseStaticMethod ( Collections2 . class )
	abstract < T , F > Collection < T > transform ( Collection < F > fromCollection , Function < ? super F , ? extends T > function ) ;

    @ UseConstructor ( AnnotationIterationFunction . class )
	abstract Function < ? super TypeElement , ? > iterationFunction ( final ProcessingEnvironment processingEnvironment , final RoundEnvironment roundEnvironment ) ;

    @ UseConstructor ( AnnotationSummaryFunction . class )
    	abstract AnnotationSummaryFunction summaryFunction ( ) ;
    //    	abstract Function < Collection < ? > , Boolean > summaryFunction ( ) ;

    @ UseInstanceMethod
	abstract < F , T > T apply ( Function < ? super F , ? extends T > function , F object ) ;
}
